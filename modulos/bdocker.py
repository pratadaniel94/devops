from flask import Blueprint, render_template, redirect
from docker import DockerClient

docker = Blueprint("docker", __name__, url_prefix='/docker')
con = DockerClient('tcp://127.0.0.1:2376')


@docker.route('')
def index():
    containers = con.containers.list(all=True)
    print([x.id for x in containers])
    return render_template('newdocker.html', containers=containers)


@docker.route("/start/<string:id>")
def start_container(id):
    con.containers.get(id).start()
    return redirect('/docker')


@docker.route("/stop/<string:id>")
def stop(id):
    con.containers.get(id).stop()
    return redirect('/docker')
