from flask import Blueprint, render_template, redirect, request
from jenkins import Jenkins, EMPTY_CONFIG_XML
from time import sleep
con = Jenkins('http://127.0.0.1:8080',
              username='danielprata',
              password='4linux'
              )

jenkins = Blueprint('jenkins', __name__, url_prefix='/jenkins')


@jenkins.route('')
def index():
    jobs = con.get_all_jobs()
    print(jobs)
    return render_template('jenkins.html', jobs=jobs)


@jenkins.route('/build/<string:name>')
def build_job(name):
    con.build_job(name)
    sleep(10)
    return redirect("/jenkins")


@jenkins.route("/update/<string:name>", methods=["GET", "POST"])
def update_job(name):
    if request.method == "GET":
        xml = con.get_job_config(name)
        return render_template('jenkins_update.html', name=name, xml=xml)
    elif request.method == "POST":
        con.reconfig_job(name, request.form['xml'])
        return redirect('/jenkins')