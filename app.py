#!/home/daniel/devops/venv/bin/python3
from flask import Flask, render_template, session, redirect, request
from modulos.bdocker import docker
from modulos.bgitlab import gitlab
from modulos.bjenkins import jenkins
from ldap3 import Connection, Server
from os import urandom

server = Server('ldap://127.0.0.1:389')
app = Flask(__name__)
app.register_blueprint(docker)
app.register_blueprint(gitlab)
app.register_blueprint(jenkins)


@app.route('/', methods=["GET", "POST"])
def index():
    if request.method == "GET":
        return render_template('index.html')
    elif request.method == "POST":
        auth = request.form
        dn = f"uid={auth['email']},dc=dexter,dc=com,dc=br"
        con = Connection(
            server,
            user=dn, password=auth['password'])
        session['auth'] = con.bind()
        if session['auth']:
            print('teste')
            return redirect('/docker')
        return redirect("/")


@app.route("/deslogar")
def deslogar():
    session['auth'] = False
    return redirect("/")


if __name__ == "__main__":
    app.secret_key = urandom(12)
    app.run(debug=True)
